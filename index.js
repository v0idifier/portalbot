const TelegramBot = require('node-telegram-bot-api')
const Koa = require('koa')
const koaBody = require('koa-body')

const db = require('bootstrap-sql')([
  `CREATE TABLE mensajes(
    id SERIAL PRIMARY KEY,
    message_id integer not null,
    from_id integer not null,
    from_first_name text not null,
    from_last_name text null,
    from_username text null,
    chat_id integer not null,
    date integer not null,
    text string not null
  );`,
])

const token = process.env.TELEGRAM_TOKEN

const bot = new TelegramBot(token, { polling: true })
const app = new Koa()

app.use(koaBody())

app.use(async ctx => {
  console.log(ctx)

  if (ctx.request.url.startsWith('/sendMessage')) {
    if (ctx.request.method === 'POST') {
      const matches = ctx.request.url.match(/\/sendMessage\/(-?[0-9]+)/)
      if (!matches) ctx.throw(400)
      let [, chatId] = matches
      chatId = parseInt(chatId)

      try {
        const msg = await bot.sendMessage(chatId, ctx.request.body.text)
        try {
          db.prepare(`INSERT INTO mensajes (
            message_id,
            from_id,
            from_first_name,
            from_last_name,
            from_username,
            chat_id,
            date,
            text
          ) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`).run(
            msg.message_id,
            msg.from.id,
            msg.from.first_name,
            msg.from.last_name,
            msg.from.username,
            msg.chat.id,
            msg.date,
            msg.text,
          )
        } catch (err) {
          console.error('dbError', err)
        }
      } catch (error) {
        console.error('teleerror ->', error)

        ctx.status = error.response.body.error_code
        ctx.body = {
          success: false,
          message: `Telegram error: ${error.response.body.description}`,
        }
        return
      }

      ctx.body = { success: true }
      return
    } else {
      ctx.throw(405)
    }
  }
  if (ctx.request.url.startsWith('/messages')) {
    if (ctx.request.method === 'GET') {
      const matches = ctx.request.url.match(/\/messages\/(-?[0-9]+)/)
      if (!matches) ctx.throw(400)
      let [, chatId] = matches
      chatId = parseInt(chatId)

      const messages = db.prepare(`
        SELECT * FROM mensajes WHERE chat_id = ?
      `).all(chatId)

      ctx.body = { success: true, messages }
      return
    } else {
      ctx.throw(405)
    }
  }

  ctx.body = `
  <style>
    .error {
      color: red;
    }
    .hidden {
      display: none;
    }

    html, body {
      margin: 0;
      padding: 0;
    }

    .things {
      display: flex;
      width: 100%;
      height: 100%;
    }

    .chats, .messages {
      display: flex;
      flex-direction: column;
    }
    .messages { flex: 1; }

    .message-list {
      flex: 1 100%;
    }

    #sendMessage {
      display: flex;
    }
    #sendMessage textarea {
      flex: 1 100%;
    }
  </style>
  <div class='things'>
    <div class='thing chats'>
      <button onclick='setChatId(-1001160695103)'>PIP público</button>
      <button onclick='setChatId(-330181393)'>chat de prueba</button>
      <input name='chatId' placeholder='chat id'>
    </div>
    <div class='thing messages'>
      <div class='message-list' id='messageList'>

      </div>
      <p class='error hidden' id='errorBox'></p>
      <form id='sendMessage'>
        <textarea name='text' placeholder='message text'></textarea>
        <button id='send'>Send!</button>
      </form>
    </div>
  </div>
  <script>
    const form = document.getElementById('sendMessage')
    const errorBox = document.getElementById('errorBox')
    const messageList = document.getElementById('messageList')
    const button = form.querySelector('#send')
    const chatIdEl = document.querySelector('input[name=chatId]')

    setChatId(chatIdEl.value)

    function setChatId (chatId) {
      chatIdEl.value = chatId
      refreshMessages(chatId)
    }

    function loading (isLoading) {
      if (isLoading) {
        button.disabled = true
        button.textContent = 'sending...'
      } else {
        button.disabled = false
        button.textContent = 'Send!'
      }
    }

    function setErr (error) {
      if (error) {
        errorBox.textContent = error.message
        errorBox.classList.remove('hidden')
      } else {
        errorBox.classList.add('hidden')
      }
    }

    async function refreshMessages (chatId) {
      const res = await fetch(\`/messages/\${chatId}\`)
      const json = await res.json()
      console.log(json)

      if (json.success) {
        messageList.innerHTML = '' // TODO: optimize
        for (const msg of json.messages) {
          console.log(msg)
          const el = document.createElement('p')
          const name = document.createElement('strong')
          name.textContent = msg.from_first_name
          if (msg.from_last_name) {
            name.textContent += ' ' + msg.from_last_name
          }
          if (msg.from_username) {
            name.textContent += ' (@' + msg.from_username + ')'
          }

          const text = document.createTextNode(' ' + msg.text)

          el.appendChild(name)
          el.appendChild(text)
          messageList.appendChild(el)
        }
      }
    }

    form.onsubmit = async evt => {
      evt.preventDefault()

      const [chatId, text] = [
        chatIdEl.value,
        evt.target.text.value,
      ]

      loading(true)

      try {
        const res = await fetch(\`/sendMessage/\${chatId}\`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            text
          }),
        })
        const json = await res.json()
        console.log(json)
        if (json.success) {
          setErr(null)
          evt.target.text.value = '' // clean
        } else {
          setErr(json)
        }
      } catch (err) {
        setErr(err)
      }

      await refreshMessages(chatId)

      loading(false)

      return false
    }
  </script>
  `
})

app.listen(3000)

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', msg => {
  const chatId = msg.chat.id
  if (msg.text) {
    try {
      db.prepare(`INSERT INTO mensajes (
        message_id,
        from_id,
        from_first_name,
        from_last_name,
        from_username,
        chat_id,
        date,
        text
      ) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`).run(
        msg.message_id,
        msg.from.id,
        msg.from.first_name,
        msg.from.last_name,
        msg.from.username,
        msg.chat.id,
        msg.date,
        msg.text,
      )
    } catch (err) {
      console.error('dbError', err)
    }
  }
  console.log('chatId:', chatId)
  //bot.sendMessage(chatId, 'Received your message')
})
